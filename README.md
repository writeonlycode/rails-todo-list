# README

This is a simple todo list with one resource, viz. **tasks**, that implements all the CRUD operations: we can **create** new tasks, as well as **read**, **update** and **delete** the existing tasks.

It was built with Ruby on Rails in the backend and Bootstrap for the CSS. You can see it in action here:

https://woc-rails-todo-list.herokuapp.com/