class TasksController < ApplicationController
  # Prefix    Verb   URI Pattern                Controller#Action
  # tasks     GET    /tasks(.:format)           tasks#index
  #           POST   /tasks(.:format)           tasks#create
  # new_task  GET    /tasks/new(.:format)       tasks#new
  # edit_task GET    /tasks/:id/edit(.:format)  tasks#edit
  # task      GET    /tasks/:id(.:format)       tasks#show
  #           PATCH  /tasks/:id(.:format)       tasks#update
  #           PUT    /tasks/:id(.:format)       tasks#update
  #           DELETE /tasks/:id(.:format)       tasks#destroy
  def index
    @tasks = Task.all.order(completed: :asc).order(id: :desc)
    @task = Task.new
    render :index
  end

  def create
    @new_task = Task.new(task_params)

    if @new_task.save
      redirect_to tasks_url
    else
      # Set error message
      redirect_to tasks_url
    end
  end

  def edit
    @task = Task.find_by(id: params[:id])
    render :edit
  end

  def update
    task = Task.find_by(id: params[:id])
    
    if task.nil?
        render plain: "Task doesn't exist", status: :unprocessable_entity
    elsif task.update(task_params)
        redirect_to tasks_url
    else
        render json: task.errors.full_messages, status: :unprocessable_entity
    end
end

  def destroy
    task = Task.find_by(id: params[:id])
        
    if task.nil?
        render plain: "Task doesn't exist", status: :unprocessable_entity
    elsif task.destroy
        redirect_to tasks_url
    else
        render json: user.errors.full_messages, status: :unprocessable_entity
    end
  end

  private
  def task_params
    params.require(:task).permit(:completed, :title, :details, :duedate)
  end
end