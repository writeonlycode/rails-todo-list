module TasksHelper
  def link_to_completed(label, task, value)
    link_to(label, task_url(task, task: { completed: value }), method: :patch, )
  end
end
