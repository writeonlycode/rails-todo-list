Rails.application.routes.draw do
  root 'tasks#index'
  get 'about' => 'pages#about'

  resources :tasks, except: [:new, :show]
end
