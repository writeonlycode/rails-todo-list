class CreateTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :tasks do |t|
      t.boolean :completed, null: false, default: false
      t.string :title, null: false
      t.text :details
      t.date :duedate

      t.timestamps
    end
  end
end
